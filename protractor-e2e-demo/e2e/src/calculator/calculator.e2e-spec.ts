import { CalculatorPage } from './calculator.po';
import * as constants from '../common/constants';

describe('Calculator Page', () => {
    let calculatorPage: CalculatorPage;

    beforeEach(() => {
        calculatorPage = new CalculatorPage();
        calculatorPage.navigateToBase();
    });

    it('Should return add result', () => {
        calculatorPage.setFirstElement(constants.fistElementNumber);
        calculatorPage.setSecondElement(constants.secondelementNumber);
        calculatorPage.clickGoButton();
        expect(calculatorPage.getResult(constants.additionRetust)).toEqual(constants.additionRetust);
    });

    afterEach(() => {

    });
});