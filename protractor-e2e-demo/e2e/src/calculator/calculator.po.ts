import { browser, by, element } from 'protractor';

export class CalculatorPage {
    navigateToBase(){
        browser.get(browser.baseUrl);
    }

    setFirstElement( firstNumber: any ){
        element(by.model('first')).sendKeys(firstNumber);
    }

    setSecondElement( secondNumber: any ) {
        element(by.model('second')).sendKeys(secondNumber);
    }

    clickGoButton(){
        element(by.id('gobutton')).click();
    }

    getResult(result: any){
        return element(by.cssContainingText('.ng-binding', result)).getText();
    }
}