# How To: Create Angular e2e Seperate Project By Protractor

## Prerequisites

- VS Code Editor
- Node.js
- NPM 
- Angular CLI
- Protractor

## 1. Open VS Code editor in your working directory.

**1.1 Run following commang in terminal**
    
   ` $ node -v `

   It will return

    ` v12.18.2 `

**1.2 Run following commang in terminal**
    
   ` $ npm -v `

   It will return

    ` 6.14.5 `

**1.3 Run following commang in terminal**
    
   ` $ ng --version `

   It will return

    ```
    Angular CLI: 10.0.3
    Node: 12.18.2
    OS: linux x64

    Angular: 
    ... 
    Ivy Workspace: 

    Package                      Version
    ------------------------------------------------------
    @angular-devkit/architect    0.1000.3
    @angular-devkit/core         10.0.3
    @angular-devkit/schematics   10.0.3
    @schematics/angular          10.0.3
    @schematics/update           0.1000.3
    rxjs                         6.5.5
    ```
**1.4 Run following commang in terminal**

   `$ protractor --version `

   It will return

    `7.0.0`

## 2. run command in terminal

`$ ng new protractor-e2e-demo `

## 3. You will get "would you like to angular routing?"

press "N" then
press Enter

## 4. You will get "Which stylesheet format would you like to use?"

Select 
`css`
press Enter

## 5. Go to

`cd protractor-e2e-demo/`

## 6. run default test 

`$ ng e2e `

## 7. Delete project "src" folder 

## 8. Open angular.json file which is in your root directory 

**8.1 got to "e2e" section** 

**8.2 remove** 

`"devServerTarget": "protractor-e2e-demo:serve" ` 

`"devServerTarget": "protractor-e2e-demo:serve:production" `

lines

## 9. create a folder name "calculator" in e2e/src

## 10. open file protractor.config.js 

## 11. change "baseUrl" 

from baseUrl: 'http://localhost:4200/',

to baseUrl: 'http://juliemr.github.io/protractor-demo/',

## 12. add followings under specs section 

```
  suites: {
    calculator : './src/calculator/*.e2e-spec.ts'
  },
```


## 13. create following files in e2e/src/calculator/ folder 

calculator.e2e-spec.ts, calculator.po.ts

## 14. create common folder in e2e/src/ folder and create following file

constants.ts

## Here is e2e directory structure

```
e2e
├── src
│   ├── calculator
│   │   ├── calculator.e2e-spec.ts
│   │   └── calculator.po.ts
│   └── common
│       └── constants.ts
├── protractor.conf.js
└── tsconfig.json
```

## 15. add following code in calculator.e2e-spec.ts

```
import { CalculatorPage } from './calculator.po';
import * as constants from '../common/constants';

describe('Calculator Page', () => {
    let calculatorPage: CalculatorPage;

    beforeEach(() => {
        calculatorPage = new CalculatorPage();
        calculatorPage.navigateToBase();
    });

    it('Should return add result', () => {
        calculatorPage.setFirstElement(constants.fistElementNumber);
        calculatorPage.setSecondElement(constants.secondelementNumber);
        calculatorPage.clickGoButton();
        expect(calculatorPage.getResult(constants.additionRetust)).toEqual(constants.additionRetust);
    });

    afterEach(() => {

    });
});
```


## 16. add following code in calculator.po.ts

```
import { browser, by, element } from 'protractor';
export class CalculatorPage {
    navigateToBase(){
        browser.get(browser.baseUrl);
    }

    setFirstElement( firstNumber: any ){
        element(by.model('first')).sendKeys(firstNumber);
    }

    setSecondElement( secondNumber: any ) {
        element(by.model('second')).sendKeys(secondNumber);
    }

    clickGoButton(){
        element(by.id('gobutton')).click();
    }

    getResult(result: any){
        return element(by.cssContainingText('.ng-binding', result)).getText();
    }
} 
```


## 17. add following code in constants.ts

```
export const fistElementNumber = '2';
export const secondelementNumber = '3';
export const additionRetust = '5';
```


## 18. run following command command in terminal to test full project

`$ ng e2e `
